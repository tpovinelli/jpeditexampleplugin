package jpplugin.jpeditexampleplugin;

import com.tom.jpedit.gui.JPEditWindow;
import com.tom.jpedit.plugins.JPEditPlugin;
import com.tom.jpedit.plugins.PluginProperties;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class JPEditExamplePlugin implements JPEditPlugin {

  @Override
  @Nullable
  public PluginProperties pluginProperties() {
    return null;
  }

  @Override
  public void onPluginLoad(List<JPEditWindow> list) {
    for (JPEditWindow window : list) {
      final List<JPEditWindow> l = List.of();
      onNewWindow(l, window);
    }
  }

  @Override
  public void onNewWindow(List<JPEditWindow> existingWindows, JPEditWindow newWindow) {
    newWindow.getTextArea().addEventHandler(KeyEvent.KEY_TYPED, event -> {
      try {
        int start, end;
        if ((start = newWindow.getTextArea().getText().indexOf("esperanto")) >= 0) {
          end = start + 9;
          newWindow.getTextArea().replaceText(start, end, ";)");
        }
        if (newWindow.getTextArea().getLength() > 200) {
          newWindow.getTextArea().setFont(Font.font(12));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
  }

  @Override
  public void onExit() {

  }
}
